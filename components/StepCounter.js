import React, { Component, useState } from 'react';
import { Button, StyleSheet, Text, View, Alert, Tab } from 'react-native';
import { Pedometer } from 'expo-sensors';
import { DataTable } from 'react-native-paper';

export default class StepCounter extends React.Component {
    state = {
        infoMessage: null,
        collectedMoney: 0,
        currentStepCount: 0,
        distance: 0,
        time: 0,
        timer: {
            enabled: false
        },
    };

    componentDidMount() {
        // console.log("stepup");
        // this._subscribe();
    }

    componentWillUnmount() {
        // this._unsubscribe();
    }
    _subscribe() {
        console.log("_subscribe");
        this.setState({ timer: { enabled: true } });
        Pedometer.getPermissionsAsync().then(
            result => {
                console.log("Access to steps is allowed");
                console.log(result);
            },
            error => {
                console.log("Access to steps is denied");
                console.log(error);
            }
        );
        this._subscription = Pedometer.watchStepCount(result => {
            console.log("stepup");
            this.setState({currentStepCount: result.steps });
            this.setState({distance: (result.steps * 0.762).toFixed(4)});
            this.setState({collectedMoney:( result.steps * 0.762 * 0.002).toFixed(4)});
            // this.setState(
            //     {
            //         currentStepCount: result.steps,
            //         distance: result.steps * 0.762,
            //         collectedMoney: result.steps * 0.762 * 0.2
            //     }
            // );
        });


        Pedometer.isAvailableAsync().then(
            result => {
                this.setState({
                    isPedometerAvailable: String(result),
                });
            },
            error => {
                this.setState({
                    isPedometerAvailable: 'Could not get isPedometerAvailable: ' + error,
                });
            }
        );

        // Pedometer.watchStepCount(result => {
        //   console.log("step ++");
        //   this.setState({
        //     currentStepCount: result.steps,
        //   });
        // });

        const end = new Date();
        const start = new Date();
        start.setDate(end.getDate() - 1);
        var secs = 1;
        this._interval = setInterval(() => {
           var newtime= this.toHHMMSS(secs++)
           this.setState({
            time: newtime
           })
          }, 1000);
    };

    toHHMMSS = (secs) => {
        var sec_num = parseInt(secs, 10)
        var hours   = Math.floor(sec_num / 3600)
        var minutes = Math.floor(sec_num / 60) % 60
        var seconds = sec_num % 60
    
        return [hours,minutes,seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v,i) => v !== "00" || i > 0)
            .join(":")
    }

    _unsubscribe = () => {
        this.setState({ timer: { enabled: false } });
        this._subscription && this._subscription.remove();
        this._subscription = null;
        clearInterval(this._interval);
    };

    recalc(steps){
        console.log("recalc");
        // this.setState({ pastStepCount: steps });
    }

    run = () => {
        console.log("run");
        if (this._subscription == null) {
            // this.setState({ timer: { enabled: true } });
            this._subscribe();
        }
        else {
            // this.setState({ timer: { enabled: false } });
            this._unsubscribe();
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Seance de BasketBall </Text>
                <View>
                    <DataTable>
                        <DataTable.Row>
                            <DataTable.Cell>{this.state.collectedMoney}</DataTable.Cell>
                            <DataTable.Cell>€</DataTable.Cell>
                            <DataTable.Cell>Argent</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                        {/* new Date(this.state.time).toLocaleTimeString() */}
                            <DataTable.Cell>{this.state.time}</DataTable.Cell>
                            <DataTable.Cell></DataTable.Cell>
                            <DataTable.Cell>Temps</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                            <DataTable.Cell>{this.state.distance} </DataTable.Cell>
                            <DataTable.Cell>m</DataTable.Cell>
                            <DataTable.Cell>Distance</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                            <DataTable.Cell>{this.state.currentStepCount}</DataTable.Cell>
                            <DataTable.Cell></DataTable.Cell>
                            <DataTable.Cell>Pas</DataTable.Cell>
                        </DataTable.Row>
                    </DataTable>
                </View>
                <Button
                    onPress={() => {
                        this.run();
                    }}
                    // disabled={!this.state.timer.enabled}
                    title={
                        !this.state.timer.enabled ? "Start" : "Stop!"
                    }
                    color={
                        !this.state.timer.enabled ? "#000080" : "#f194aa"
                    }
                />
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        paddingTop: 100,
        paddingHorizontal: 30,
    },
});