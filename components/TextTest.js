import React, { Component,useState  } from 'react';
import { Button,Text, TextInput, View } from 'react-native';

const TextTest = (props) => {
  return (
    <View>
      <Text>Hello, I am.{props.name}..</Text>
      <TextInput
        style={{
          height: 40,
          borderColor: 'gray',
          borderWidth: 1
        }}
        defaultValue="Name me!"
      />
    </View>
  );
}

class Cat extends Component {
    state = { isHungry: true };
    render() {
      return (
        <View>
          <Text>
            I am {this.props.name}, and I am
            {this.state.isHungry ? " hungry" : " full"}!
          </Text>
          <Button
            onPress={() => {
              this.setState({ isHungry: false });
            }}
            disabled={!this.state.isHungry}
            title={
              this.state.isHungry ? "Pour me some milk, please!" : "Thank you!"
            }
          />
        </View>
      );
    }
}

const PizzaTranslator = () => {
    const [text, setText] = useState('');
    return (
      <View style={{padding: 10}}>
        <TextInput
          style={{height: 40}}
          placeholder="Type here to translate!"
          onChangeText={text => setText(text)}
          defaultValue={text}
        />
        <Text style={{padding: 10, fontSize: 42}}>
          {text.split(' ').map((word) => word && '🍕').join(' ')}
        </Text>
      </View>
    );
  }

export { TextTest , Cat, PizzaTranslator}