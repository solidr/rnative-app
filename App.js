import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {TextTest,Cat,PizzaTranslator} from './components/TextTest'
import StepCounter from './components/StepCounter'
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
    {/* <View style={styles.container}> */}
      {/* <StatusBar style="auto" /> */}
      {/* <TextTest name="Murzik"/> */}
      {/* <Cat/> */}
      <StepCounter/>
      {/* <PizzaTranslator/> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    justifyContent: 'center',
  },
});
